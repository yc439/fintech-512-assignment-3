//package kwic_test;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer; 

public class Kwic {
	private	List<String> stopWords = new ArrayList<String>(); 
	private  ArrayList<String> kwicList=new ArrayList<String>(); 
	private  ArrayList<String> lineTxt=new ArrayList<String>();
	public static void main(String[] args) {
       		Kwic kwic1=new Kwic();
       		kwic1.input( );
       		kwic1.shift();
       		kwic1.alphabetizer();
       		kwic1.output();
	}
	//class to read stdin
	public  void input() {
		Scanner scanner = new Scanner(System.in);
		while (true){
			String keyword = scanner.nextLine();
			if (!keyword.equals("::")) {
				stopWords.add(keyword.toLowerCase());
			}else {
				break;
			}
		}
		while (scanner.hasNext()) {
			String line = scanner.nextLine();
			lineTxt.add(line.toLowerCase());		
		}	
	}
	//class to print out the result
	public void output(){
		Iterator<String> it=kwicList.iterator();
		while(it.hasNext()){
			String str = it.next();
			int index =str.indexOf(" ");
			String str2 = str.substring(index+1);
			System.out.println(str2);
		}
	}
	//class to shift keyword for each line and set it to uppercase
	public  void shift(){
		//get each word and store it in tokens
		Iterator<String> it=lineTxt.iterator();
		while(it.hasNext()){
			StringTokenizer token=new StringTokenizer(it.next());
			ArrayList<String>tokens=new ArrayList<String>();
			int i=0;
			int count=token.countTokens();
			while(i<count){
				tokens.add(token.nextToken());
				i++;      	
			}
			//ignore stopwords and set the keyword to uppercase by loop
			for(i=0;i<count;i++){
				StringBuffer lineBuffer=new StringBuffer();
				if(!stopWords.contains(tokens.get(i))) {
					lineBuffer.append(tokens.get(i));
					lineBuffer.append(" ");
					for(int index =0; index<count; index++) {
						if(index==i) {
							lineBuffer.append(tokens.get(index).toUpperCase());
						}else {
							lineBuffer.append(tokens.get(index));	
						}
						lineBuffer.append(" ");		
					}
					String tmp=lineBuffer.toString();
					kwicList.add(tmp);
				}
			}
		}
	}

	public  void alphabetizer(){
		Collections.sort(this.kwicList,new AlphabetizerComparator());
	}
	private class AlphabetizerComparator implements Comparator<String>{
		@Override
		public int compare(String o1,String o2){
			if(o1==null&&o2==null){
				throw new NullPointerException();	
			}
			int compareValue=0;
			int i = 0;
			while(compareValue==0) {
				char o1c=o1.charAt(i);	
				char o2c=o2.charAt(i);
				compareValue=o1c-o2c;	
				if(compareValue==0&&i==o1.indexOf(" ")) {
					break;
				}
				i++;
			}
			return compareValue;
		}
	}
}


